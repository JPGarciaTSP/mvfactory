import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Clima } from 'src/app/models/Clima';
import { ClimaService } from 'src/app/services/clima.service';
@Component({
  selector: 'app-clima',
  templateUrl: './clima.component.html',
  styleUrls: ['./clima.component.css']
})

export class ClimaComponent implements OnInit {
 form: FormGroup;  
 clima: Clima = {};
 list:Array<Clima> = [];

  constructor(private formBuilder: FormBuilder, private climaService: ClimaService) {
      this.form = this.formBuilder.group({
        id: 0,
        selectProv: ['', [Validators.required]],
        historico: [''],
        ciudad: [''],        
      })
   }

  ngOnInit() {
    
  }

  showResults(){     
    
      this.list = [];  
      this.climaService.getClima(this.form.get('selectProv')?.value.toString()).subscribe((response)=> {      
      this.clima.ciudad = this.form.get('selectProv')?.value.toString();       
      this.clima.pais = "Argentina";      
      this.clima.temperatura = (response["main"]["temp"] - 273.15).toFixed().toString();
      this.clima.sensasionTermica = (response["main"]["feels_like"] - 273.15).toFixed().toString();      
      });   
      this.list.push(this.clima);          
      if(this.form.get("historico")?.value == true){
          this.getHistory(this.form.get("selectProv")?.value.toString());
      }
      this.saveHistory(this.clima); 
      this.clima.ip = "123.123.22"; //aquí iria el dato de la persona que estaría viendo el clima, para mostrar solo su historial         
  }

  saveHistory(clima: Clima){    
    this.climaService.saveHistoryToApi(clima).subscribe(data => {
      console.log("guardado con éxito");
    });    
  }

  getHistory(ciudad: String){
    var clima: Clima = {};
    this.climaService.getHistorial().subscribe((response: Array<any>)=> {
      for(let i = 0; i < response.length; i++) {
        if(response[i].ciudad == ciudad){
          clima.ciudad = response[i].ciudad; 
          clima.ip = response[i].ip;
          clima.pais = response[i].pais;
          clima.temperatura = response[i].temperatura;
          clima.sensasionTermica = response[i].sensasionTermica;
          this.list.push(clima);
        }        
      }     
    });       
  }
}
