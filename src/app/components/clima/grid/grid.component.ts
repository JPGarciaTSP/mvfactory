import { Component, OnInit } from '@angular/core';
import { ClimaService } from 'src/app/services/clima.service';
import { ClimaComponent } from '../clima.component';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit {

  constructor(public climaService: ClimaService, public clima: ClimaComponent) { }

  ngOnInit(): void {
    
  }

}
