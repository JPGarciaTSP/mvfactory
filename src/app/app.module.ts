import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http'

import { AppComponent } from './app.component';
import { ClimaComponent } from './components/clima/clima.component';
import { GridComponent } from './components/clima/grid/grid.component';
import { ResultadoComponent } from './components/clima/resultado/resultado.component';

@NgModule({
  declarations: [
    AppComponent,
    ClimaComponent,
    GridComponent,
    ResultadoComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
