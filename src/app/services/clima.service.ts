import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Clima } from "../models/Clima";

@Injectable ({
    providedIn: 'root'
})

export class ClimaService{
        
    myAppUrl = "https://localhost:44397/";
    myApiUrl = "api/Historial";    
    public return: any; 
    

    constructor(private http: HttpClient){

    } 
    

    public saveHistoryToApi(clima: Clima): Observable<Clima>{
        return this.http.post<Clima>(this.myAppUrl + this.myApiUrl, clima);
    }

    public getClima(ciudad: string){

      return this.http.get<any>("https://api.openweathermap.org/data/2.5/weather?q="+ ciudad +",AR&APPID=f6a93e0ec06e59f95f60cea80b12f860");      

      } 

    public getHistorial(){
      return this.http.get<any>(this.myAppUrl + this.myApiUrl);
    }
}
